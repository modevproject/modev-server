Rails.application.routes.draw do
  devise_for :users
  resources :members, as: :users
  resources :members
  resources :events
  resources :locations
  resources :social

  resources :venues
  resources :sponsors
  resources :social_events
  resources :speakers
  resources :event_sessions
  resources :skill_sets
  resources :tickets  do    
    get 'activate', on: :member
  end

    namespace :admin do
      resources :home
    end
    namespace :api do
      namespace :v1 do
        devise_for :users
        resources :speakers
        resources :auth_test
        resources :event_sessions
        resources :events
        resources :tokens
        resources :check_ins
        resources :locations
        resources :messages
        resources :connections
        resources :tickets
        resources :ratings
        resources :venues
        resources :sponsors
        resources :social_events
        resources :skill_sets
        resources :profile do
            put 'photo_upload', on: :member 
            post 'add_session', on: :member        
            delete 'delete_session', on: :member        
        end
        resources :attendees
      end
    end
  # root 'home#index'
  root 'dash#index'
  get 'ping_api' => 'ping_api#index'

  

end

module DateReader

	def friendly_d d
		DateReader::convert_datetime_to_readable_description d
	end

	
	def self.convert_datetime_to_readable_description unfriendly
		now = DateTime.current

		case unfriendly.to_i
			when ((now - 1.hour).to_i)..(now.to_i)
				"Just now"
			when ((now - 3.hour).to_i)..(now.to_i)
				"Few Moments Ago"
			when ((now.beginning_of_day).to_i)..(now.to_i)
				"Today"
			when ((now.yesterday).to_i)..(now.to_i)
				"Yesterday"
			when ((now.yesterday.yesterday).to_i)..(now.to_i)
				"2 Days ago"
			when ((now.beginning_of_week).to_i)..(now.to_i)
				"Earlier This Week"
			when ((now.prev_week).to_i)..(now.to_i)
				"Last Week"
			when ((now - 2.week).to_i)..(now.to_i)
				"A couple Weeks Ago"
			when ((now.beginning_of_month).to_i)..(now.to_i)
				"Earlier This Month"
			when ((now - 4.week).to_i)..(now.to_i)
				"A few Weeks Ago"
			when ((now.prev_month).to_i)..(now.to_i)
				"Last Month"
			when ((now.beginning_of_year).to_i)..(now.to_i)
				"This year"
			when ((now.prev_year).to_i)..(now.to_i)
				"Last Year"
			else
				"Long Time ago"
		end
	end

end
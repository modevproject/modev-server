class MembersController < ApplicationController
	layout 'dash'
	before_action :is_user_logged_in?
	def index
		@member = User.new

		@members = User.all
		# @friendly = DateReader.friendly_date
	end
	def new
		@member = User.new
	end
	def show
		render nil
	end
	def create
		@members = User.all
		begin
			@member = User.new user_params
			@member.save!
			flash[:notice] = "User Created!"
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			render 'index'
		else
			redirect_to members_path
		end
	end
	def edit
		unless @member = User.find(params['id'])
			flash[:notice] = "User Does not exist"
			redirect_to members_path
		end
	end
	def update
		unless @member = User.find(params['id'])
			flash[:notice] = "User Does not exist"
			redirect_to members_path
		end
		if @member.update_attributes user_params
			flash[:notice] = "User Updated"
			redirect_to members_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_member_path(@member)
		end
	end
	def destroy
		if @member = User.find(params['id']) then
			@member.destroy
			flash[:notice] = "User Deleted!"
		end
		redirect_to members_path
	end
	private
	def user_params
		if params["user"]["password"].blank?
		params.require(:user).permit( :first_name, :last_name, :title, :company, :avatar, :email) 
		else
		params.require(:user).permit( :first_name, :last_name, :title, :company, :avatar, :email, :password) 
		end
	end
end

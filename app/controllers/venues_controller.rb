class VenuesController < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@venues = Venue.all
	end
	def new
		@venue = Venue.new
	end
	def show
		begin
		@venue = Venue.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to venues_path
		end
	end
	def edit
		begin
		@venue = Venue.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to venues_path
		end
	end
	def create
		@venue = Venue.new(venue_params)
		begin
			@venue.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Venue Created!"
			redirect_to venues_path
		else
			render 'new'
		end
	end
	def update
		unless @venue = Venue.find(params['id'])
			flash[:notice] = "Venue Does not exist"
			redirect_to venue_index_path
		end
		if @venue.update_attributes venue_params
			flash[:notice] = "Venue Updated"
			redirect_to venues_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_venue_path(@venue)
		end
	end
	def destroy
		if @venue = Venue.find(params['id']) then
			@venue.destroy
			flash[:notice] = "Venue Deleted!"
		end
		redirect_to venues_path
	end
	private
	def venue_params
		params.require(:venue).permit(:title,:body)
	end
end

class EventsController < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?

def index
		@events = Event.all
	end
	def new
		@event = Event.new
	end
	def show
		begin
		@event = Event.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to events_path
		end
	end
	def edit
		begin
		@event = Event.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to events_path
		end
	end
	def create
		@event = Event.new(event_params)
		begin
			@event.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Event Created!"
			redirect_to events_path
		else
			render 'new'
		end
	end
	def update
		unless @event = Event.find(params['id'])
			flash[:notice] = "Event Does not exist"
			redirect_to event_index_path
		end
		if @event.update_attributes event_params
			flash[:notice] = "Event Updated"
			redirect_to events_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_event_path(@event)
		end
	end
	def destroy
		if @event = Event.find(params['id']) then
			@event.destroy
			flash[:notice] = "Event Deleted!"
		end
		redirect_to events_path
	end
	private
	def event_params
		params.require(:event).permit(:name,:start_date,:end_date,:address1,:address2,:zip,:state,:country)
	end
end

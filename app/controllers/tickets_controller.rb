class TicketsController < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@csv_files = CsvFile.all
		@csv_file = CsvFile.new
		@registred = TicketRegistration.all
		# @first = first_line
		# @rows = []
		# ::CSV.foreach(@tickets.first.csv_file.path,:headers => true, :converters => :all) do |row|
		# 	reg_ticket = TicketRegistration.build_me row.to_hash
		# 	begin
		# 	# reg_ticket.save!
		# 	rescue Exception => e
		# 		flash[:error] = e
		# 	end
		# 	if flash[:error].nil?
		# 		flash[:notice] = "Shop Created!"
		# 	end
		# end
	end

	def create
		@csv_files = CsvFile.all
		@csv_file = CsvFile.new(csv_file_params)
		begin
			@csv_file.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Shop Created!"
			redirect_to tickets_path
		else
			render 'index'
		end
	end

	def update
		unless @csv_file = CsvFile.find(params['id'])
			flash[:notice] = "User Does not exist"
			redirect_to tickets_path
		end
		if @csv_file.update_attributes csv_file_params
			flash[:notice] = "Csv Updated"
			redirect_to tickets_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_ticket_path(@csv_file)
		end
	end	

	def edit
		unless @csv_file = CsvFile.find(params['id'])
			flash[:notice] = "Csv file Does not exist"
			redirect_to tickets_path
		end
	end

	def destroy
		if @csv_file = CsvFile.find(params['id']) then
			@csv_file.destroy
			flash[:notice] = "User Deleted!"
		end
		redirect_to tickets_path
	end

	def activate
		if @csv_file = CsvFile.find(params['id']) then
			if @csv_file.is_active then
				@csv_file.is_active = false
				remove_tickets @csv_file.file.path
				flash[:notice] = "De-activated"
			else
				@csv_file.is_active = true
				add_tickets @csv_file.file.path
				flash[:notice] = "Activated!"
			end
		end
		@csv_file.save!
			redirect_to tickets_path	
	end


	private
	def csv_file_params
		params.require(:csv_file).permit(:note,:file)
	end

	def add_tickets file_path
		::CSV.foreach(CsvFile.first.file.path,:headers => true, :header_converters => :symbol, :converters => :all) do |row|
			reg_ticket = TicketRegistration.build_me row.to_hash
			begin
				reg_ticket.save!
			rescue Exception => e
				flash[:error] = e
			end
			if flash[:error].nil?
				flash[:notice] = "Added files!"
			end
		end
	end
	def remove_tickets file_path
		::CSV.foreach(file_path,:headers => true, :header_converters => :symbol, :converters => :all) do |row|
			reg_ticket = TicketRegistration.find_me row.to_hash
			begin
				reg_ticket.destroy!
			rescue Exception => e
				flash[:error] = e
			end
			if flash[:error].nil?
				flash[:notice] = "destroyed files!"
			end
		end
	end
	def first_line
		me = TicketRegistration.new
			num = 1
		::CSV.foreach(CsvFile.first.file.path,:headers => true, :header_converters => :symbol, :converters => :all) do |row|
			if num == 4

			TicketRegistration.get_structure.each do |key, value|
			# ticket[key] = row[value]
			# me = row.to_hash["my_current_position_is"]
			me[key] = row[value]
			# me << row[value]
			end
			break
			end
			num += 1
		end
		me.save!
		me.inspect
	end

end
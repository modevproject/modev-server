class Admin::HomeController < ApplicationController
	protect_from_forgery with: :null_session
	def index
	render :json => user_params
	end
	def create
	    user = User.new(params[:user])
	    if user.save
	      render :json=> user.as_json(:email=>user.email), :status=>201
	      return
	    else
	      warden.custom_failure!
	      render :json=> user.errors, :status=>422
	    end
	end

	def user_params
    params.require(:user).permit(:email, :password)
    end
end

class SkillSetsController < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@skill_sets = SkillSet.all
	end
	def new
		@skill_set = SkillSet.new
	end
	def show
		begin
		@skill_set = SkillSet.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to skill_sets_path
		end
	end
	def edit
		begin
		@skill_set = SkillSet.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to skill_sets_path
		end
	end
	def create
		@skill_set = SkillSet.new(skill_set_params)
		begin
			@skill_set.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "SkillSet Created!"
			redirect_to skill_sets_path
		else
			render 'new'
		end
	end
	def update
		unless @skill_set = SkillSet.find(params['id'])
			flash[:notice] = "SkillSet Does not exist"
			redirect_to skill_set_index_path
		end
		if @skill_set.update_attributes skill_set_params
			flash[:notice] = "SkillSet Updated"
			redirect_to skill_sets_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_skill_set_path(@skill_set)
		end
	end
	def destroy
		if @skill_set = SkillSet.find(params['id']) then
			@skill_set.destroy
			flash[:notice] = "SkillSet Deleted!"
		end
		redirect_to skill_sets_path
	end
	private
	def skill_set_params
		params.require(:skill_set).permit(:name)
	end
end

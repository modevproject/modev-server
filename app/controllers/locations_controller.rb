class LocationsController < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@locations = Location.all
	end
	def new
		@location = Location.new
	end
	def show
		begin
		@location = Location.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to locations_path
		end
	end
	def edit
		begin
		@location = Location.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to locations_path
		end
	end
	def create
		@location = Location.new(location_params)
		begin
			@location.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Location Created!"
			redirect_to locations_path
		else
			render 'new'
		end
	end
	def update
		unless @location = Location.find(params['id'])
			flash[:notice] = "Location Does not exist"
			redirect_to location_index_path
		end
		if @location.update_attributes location_params
			flash[:notice] = "Location Updated"
			redirect_to locations_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_location_path(@location)
		end
	end
	def destroy
		if @location = Location.find(params['id']) then
			@location.destroy
			flash[:notice] = "Location Deleted!"
		end
		redirect_to locations_path
	end
	private
	def location_params
		params.require(:location).permit(:name)
	end
end

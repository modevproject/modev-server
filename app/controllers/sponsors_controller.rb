class SponsorsController  < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@sponsors = Sponsor.all
	end
	def new
		@sponsor = Sponsor.new
	end
	def show
		begin
		@sponsor = Sponsor.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to sponsors_path
		end
	end
	def edit
		begin
		@sponsor = Sponsor.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to sponsors_path
		end
	end
	def create
		@sponsor = Sponsor.new(sponsor_params)
		begin
			@sponsor.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Sponsor Created!"
			redirect_to sponsors_path
		else
			render 'new'
		end
	end
	def update
		unless @sponsor = Sponsor.find(params['id'])
			flash[:notice] = "Sponsor Does not exist"
			redirect_to sponsor_index_path
		end
		if @sponsor.update_attributes sponsor_params
			flash[:notice] = "Sponsor Updated"
			redirect_to sponsors_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_sponsor_path(@sponsor)
		end
	end
	def destroy
		if @sponsor = Sponsor.find(params['id']) then
			@sponsor.destroy
			flash[:notice] = "Sponsor Deleted!"
		end
		redirect_to sponsors_path
	end
	private
	def sponsor_params
		params.require(:sponsor).permit(:title,:body)
	end
end
class SpeakersController < ApplicationController
	include ActionView::Helpers::TextHelper
	layout "dash"
	before_action :is_user_logged_in?
	before_action :format_description, :only => [:create, :update]

def index
		@speakers = Speaker.all
	end
	def new
		@speaker = Speaker.new
	end
	def show
		begin
		@speaker = Speaker.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to speakers_path
		end
	end
	def edit
		begin
		@speaker = Speaker.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to speakers_path
		end
	end
	def create
		@speaker = Speaker.new(speaker_params)
		begin
			@speaker.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Speaker Created!"
			redirect_to speakers_path
		else
			render 'new'
		end
	end
	def update
		unless @speaker = Speaker.find(params['id'])
			flash[:notice] = "Speaker Does not exist"
			redirect_to speaker_index_path
		end
		if @speaker.update_attributes speaker_params
			flash[:notice] = "Speaker Updated"
			redirect_to speakers_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_speaker_path(@speaker)
		end
	end
	def destroy
		if @speaker = Speaker.find(params['id']) then
			@speaker.destroy
			flash[:notice] = "Speaker Deleted!"
		end
		redirect_to speakers_path
	end
	private
	def speaker_params
		params.require(:speaker).permit(:first_name,:last_name,:title,:description,:company,:profile_image,:twitter,:user_id)
	end
	def format_description
		params["speaker"]["description"] = simple_format(params["speaker"]["description"]);
	end
end

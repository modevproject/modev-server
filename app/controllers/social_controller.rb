# require 'net/http'
# require 'net/https'
class SocialController < ApplicationController
	def index
		if params["code"].nil?
			render :json => {:JUST => params.as_json}
		else
		begin
		oauth = LinkedIn::OAuth2.new
		@state = params["state"]
		access_token = oauth.get_access_token(params["code"])
		render :json => {:token => access_token, :state => @state}
		rescue Exception => e
		render :json => {:codehere => e.as_json}
		end
		end
	end
# private
# 	def get_api_call
# 		#.parse
# 	    uri = URI("https://www.linkedin.com/uas/oauth2/accessToken")
# 	    uri.query = URI.encode_www_form(what_args_you_want_to_send)

# 	    http = Net::HTTP.new(uri.host, uri.port)
# 	    http.use_ssl = true
# 	    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
# 	    request = Net::HTTP::Post.new(uri.request_uri)
# 	    http.request(request).body
# 	end

# 	def what_args_you_want_to_send
# 	{
# 	      "grant_type" => 'authorization_code',
# 	      "code" => params["code"],
# 	      "redirect_uri" => 'http%3A%2F%2Fmodev.appteam.io%2Fsocial%2F',
# 	      "client_id" => '77qsd08fxntxnk',
# 	      "client_secret" => 'lOx165KOEVObOsYQ'
	      
# 	}
# 	end
end

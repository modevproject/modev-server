class Api::V1::SkillSetsController < Api::V1::BaseApiController
	before_action :is_token_valid?
	def index
		@skill_sets = SkillSet.all
		render :json => @skill_sets.as_json
	end
end

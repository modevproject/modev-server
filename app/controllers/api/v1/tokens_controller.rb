require 'net/http'
require 'net/https'
class Api::V1::TokensController < ApplicationController
	def index
		if params["code"].nil?
			render :json => {:JUST => params.as_json}
		elsif params["state"].nil?
			render :json => {:state => 'not available'}
		else
			begin
				oauth = LinkedIn::OAuth2.new
				access_token = oauth.get_access_token(params["code"])
				oauth_params = "oauth2_access_token="+access_token.token
				format= "format=json"
				url_params = "?"+oauth_params+"&"+format
				complete_url = "https://api.linkedin.com/v1/people/~:(id,first-name,positions,summary,last-name,email-address,picture-url)"+url_params
				uri = URI(complete_url)
			    http = Net::HTTP.new(uri.host, uri.port)
			    http.use_ssl = true
			    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			    request = Net::HTTP::Get.new(uri.request_uri)
			    linkedin_response = ActiveSupport::JSON.decode(http.request(request).body)

			    user = User.where(email: linkedin_response["emailAddress"]).first
			    # user = nil
			    # #need to add user sign up option so that user can only user one
			    if user.nil?
			    	pass = '161849811'
			    	title = nil
			    	company = nil
			    	avatar = nil
			    	avatar = URI.parse(linkedin_response["pictureUrl"]) unless linkedin_response["emailAddress"].blank?
			    	for position in linkedin_response["positions"]["values"]
			    		if position["isCurrent"]
			    		company = position["company"]["name"]
			    		title = position["title"]
			    		end
			    	end
			     user = User.new(:email => linkedin_response["emailAddress"],
				    			 :first_name => linkedin_response["firstName"],
				    			 :last_name => linkedin_response["lastName"],
				    			 :password => pass,
				    			 :title => title,
				    			 :company => company,
				    			 :avatar => avatar,
				    			 :sign_up_type => "linkedin")


				    # 			 :description => linkedin_response["emailAddress"],
				@token = Token.new :state => params["state"], :token => access_token.token
				user.token = @token
				    if user.save
				    	user.reload
			    		render :json => {:user => user,:token => company ,:avatar => avatar, :data =>  linkedin_response}
			    	else
			    		raise 'could not save user'
			    	end
			    else
			    	@token = Token.new :state => params["state"], :token => access_token.token
					user.token = @token
					user.save
				    if user.sign_up_type != "linkedin"
				    	raise "You can only sign in with the original method you used"
				    end
			    	render :json => {:user => 'ok'}
			    end
				# render :json => {:success=>linkedin_response["emailAddress"]}

			#     end

			# 	# @state = params["state"]
			# 	# oauth = LinkedIn::OAuth2.new
			# 	# access_token = oauth.get_access_token(params["code"])
			# 	# @token.save!
			# 	# render :json => {:state => @state, :token => access_token["token"]}
			rescue Exception => e
				render :json => {:codehere => e.message}
			end
		end
	end

	def show
		begin
			@token = Token.where(:state => params['state']).first
			raise 'Token not found' unless !@token.nil?
			render :json => @token.user.as_json(:include => [:sessions,:ratings])
		rescue Exception => e
			render :json=> {:success=>false, :message=>'Token not found'}, :status=>401
		end
	end
end

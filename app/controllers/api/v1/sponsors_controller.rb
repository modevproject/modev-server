class Api::V1::SponsorsController  < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@sponsors = Sponsor.all
		render :json => @sponsors.as_json
	end
end

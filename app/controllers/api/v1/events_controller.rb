class Api::V1::EventsController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@events = Event.all
		render :json => @events.as_json
	end
	def show
		begin
		@event = Event.find(params['id'])
		render :json => @event.as_json
		rescue Exception => e
		render :json=> {:success=>false, :message=>"Error Event not found"}, :status=>401
		end
	end
end


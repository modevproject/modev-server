class Api::V1::AttendeesController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@attendees = User.all
		render :json => @attendees.as_json
	end
end

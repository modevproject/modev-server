class Api::V1::BaseApiController < ActionController::Base
	respond_to :json 
	protect_from_forgery with: :null_session
	
	private
	def is_token_valid?
		@user = User.find_by_api_token request.params["token"]
		render :json => {:error=> 'no valid token'} unless !@user.nil?
	end
	def format_sessions sessions
		event_session_formated = Array.new
		sessions.each do |session|
			if(!session.location.nil?)
		    	session.location_name = session.location.name
		    end
			session.speakers.each do |speaker|
		    	session.speakers_list << {:id => speaker.id, :name => speaker.name, :image => speaker.profile_image_url}
		    end
		    session.checked_in_users.each do |user|
		      session.checked_in_users_list << {:id => user.id,
		      								 :name => user.name,
		      								  :image => user.profile_image_url}
		    end
		event_session_formated << session
		end
		event_session_formated
	end
end

class Api::V1::VenuesController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@venues = Venue.all
		render :json => @venues.as_json
	end
end

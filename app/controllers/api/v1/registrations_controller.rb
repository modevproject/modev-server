class Api::V1::RegistrationsController < Api::V1::BaseDeviseApiController
	respond_to :json
  
  def create
    user = User.new(permitted_params)
    user.api_token = generate_authentication_token
    if user.save
      render :json=> user.as_json(:auth_token=>user.api_token, :email=>user.email), :status=>201
      return
    else
      warden.custom_failure!
      render :json=> user.errors, :status=>422
    end
  end

  private
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(api_token: token).first
    end
  end
  def permitted_params
  params.require(:user).permit(:email, :password, :first_name, :last_name)
  end
end

class Api::V1::AuthTestController < Api::V1::BaseApiController

	# before_action :is_token_valid?
	before_action :check_Location
	def index
		result = request.location
		render :json => {:profile2 => result}
	end
	def create
		render :json => {:profile2 => '@user'}
		response.headers['Access-Control-Allow-Origin'] = '*'
		response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
		response.headers['Content-Type'] = 'application/json'
		# response.headers['X-Frame-Options'] = 'application/json'
		remove_keys = %w(X-Frame-Options)
		response.headers.delete_if{|key| remove_keys.include? key}

	end
	def check_Location
		result = request.location
		render :json => {:error=> 'no a valid country',:country_code => result.country_code} unless result.country_code == 'US'
	end

end

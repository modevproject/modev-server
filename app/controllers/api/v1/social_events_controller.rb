class Api::V1::SocialEventsController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@social_events = SocialEvent.all
		render :json => @social_events.as_json
	end
end

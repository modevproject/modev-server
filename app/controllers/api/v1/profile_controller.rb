class Api::V1::ProfileController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@users = User.includes(:sessions).all
		render :json => @users.as_json(:include => [:sessions,:ratings])
	end
	def show
		begin
			@user = User.find(params['id'])
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			@final_user = User.includes(:sessions).find(params['id'])
			render :json=> { :success => true, :user => @final_user.as_json(:include => [:sessions,:ratings]) }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find profile' }, :status=>422
		end
	end
	def update
		# params['avatar'] = decode_picture_data "params['base64']['profile_image']"
		begin
			@user = User.find(params['id'])
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			if @user.update_attributes user_params
				@final_user = User.includes(:sessions).find(params['id'])
			render :json=> { :success => true, :user => @final_user.as_json(:include => [:sessions,:ratings]) }, :status=>201
			else
				render :json=> { :success => false, :error => 'could not update' }, :status=>422
			#need to add token verification
			end
			# render :json=> { :success => true, :user => @user }, :status=>201
		else
			render :json=> {:success=>false, :message=>error}, :status=>422
		end
	end
	def photo_upload
		begin
			@user = User.find(params['id'])
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			if update_avatar
				@final_user = User.includes(:sessions).find(params['id'])
			render :json=> { :success => true, :user => @final_user.as_json(:include => [:sessions,:ratings]) }, :status=>201
			else
				render :json=> { :success => false, :error => 'could not update photo' }, :status=>422
			#need to add token verification
			end
		end
	end
	def add_session
		begin
			@user = User.find(params['id'])
			@event = EventSession.find(params['session'])
			if !@user.sessions.include? @event
				@user.sessions << @event
				@user.save
			else
				raise 'already in scedual'
			end
		rescue Exception => e
			error = e.to_json
		end
		if error.nil?
			@final_user = User.includes(:sessions).find(params['id'])
			render :json=> { :success => true, :user => @final_user.as_json(:include => [:sessions,:ratings]) }, :status=>201
		else
				render :json=> { :success => false, :error => 'could not add session' }, :status=>422
		end
	end
	def delete_session
		begin
			@user = User.find(params['id'])
			@event = EventSession.find(params['session'])
			if @user.sessions.include? @event
				@user.sessions.delete(@event)
				@user.save
			else
				raise 'cannot delete'
			end
		rescue Exception => e
			error = e.to_json
		end
		if error.nil?
				@final_user = User.includes(:sessions).find(params['id'])
			render :json=> { :success => true, :user => @final_user.as_json(:include => [:sessions,:ratings]) }, :status=>201
		else
				render :json=> { :success => false, :error => 'could not delete session' }, :status=>422
		end
	end
	private
	def user_params
		params.require(:user).permit(:first_name,:last_name,:title,:company,:avatar,:skills,:description)
	end
	def update_avatar
		if !params['base64']['profile_image'].nil?
			@user.avatar = decode_picture_data params['base64']['profile_image']
			@user.save
			@user.reload
		end
	end
	def decode_picture_data picture_data
    # decode the base64
    data = StringIO.new(Base64.decode64(picture_data))

    # assign some attributes for  processing
    data.class.class_eval { attr_accessor :original_filename, :content_type }
    data.original_filename = params['base64']['file_name']
    data.content_type = "image/jpeg"

    # return decoded data
    data
	end
end

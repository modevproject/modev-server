class Api::V1::TicketsController  < Api::V1::BaseApiController
	before_action :is_token_valid?
	def index
		begin
		@ticket = TicketRegistration.where(:email => params['email']).first
		raise 'Ticket not found' unless !@ticket.nil?
		# raise 'Ticket not found' unless @ticket.nil?
		render :json => @ticket.as_json
		rescue Exception => e
		render :json=> {:success=>false, :message=>'Ticket not found'}, :status=>401
		end
	end
end

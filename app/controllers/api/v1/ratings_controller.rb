class Api::V1::RatingsController < Api::V1::BaseApiController
	# before_action :is_token_valid?
	def show
		#to do
	end
	def create
		begin
		if params['rate'].to_i > 5
				raise 'rating error'
		end
		if EventSession.find(params['session'].to_i).blank?
				raise 'event does not exist'
		end
		@user = User.includes(:ratings).find(params['user'].to_i)
		@new_rating = true
		@user.ratings.each do |rating|
		      if rating.event_session_id == params['session'].to_i
					@new_rating = false
					@rating = Rating.find(rating.id)
					@rating.score = params['rate']
					@rating.save
			  end
		end
		if @new_rating
			session_id = params['session']
			rate = params['rate']
			@rated = Rating.new :score => rate ,:user => @user, :event_session_id => session_id
			@rated.save
		end
		@fresh_user = User.includes(:ratings,:sessions).find(params['user'].to_i)
		rescue Exception => e
			error = 'Rating error'
		end
		if error.nil?
			render :json=> { :success => true,
			 :user =>  @fresh_user.as_json(:include => [:sessions,:ratings]),
			 }, :status=>201
		else
			render :json=> { :success => false,
			 :error => 'could not find user' }, :status=>422
		end
		
		# @user = User.includes(:rating).find(params['user'])
		# @session = EventSession.find(params['session'])
		# @new_rating = true
		# @user.ratings.each do |rating|
		#       if rating.session.id == @session.id
		# 			@new_rating = false
		# 			rating.score = params['rate']
		# 			rating.update
		# 	  end
		# end
		# if @new_rating
		# 	@rated = Rating.new :score => params['rate'] ,:user => @user, :session => @session
		# 	@rated.save
		# end

	end
end

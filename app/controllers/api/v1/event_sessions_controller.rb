class Api::V1::EventSessionsController < Api::V1::BaseApiController
	# before_action :is_token_valid?

	def index
		@event_sessions = EventSession.includes(:location,:checked_in_users,:speakers).all
		@sessions = format_sessions @event_sessions

		render :json => @sessions.as_json
	end
	def show
		#show user sessions here
	end
end

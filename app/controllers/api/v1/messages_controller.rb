class Api::V1::MessagesController < Api::V1::BaseApiController
	# before_action :is_token_valid?
	def show
		begin
			@user = User.find(params['id'])
			messages = {:received =>@user.received_messages.order(created_at: :desc).limit(30),
						:sent =>nil}
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			render :json=> { :success => true, :messages => messages }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find user' }, :status=>422
		end
	end
	def create
		@message = Message.new(message_params)
		begin
			@message.save!
			@user = User.find(params['message']['sender_id'])
			messages = {:received =>@user.received_messages.order(created_at: :desc).limit(30),
						:sent =>@user.sent_messages.order(created_at: :desc).limit(30)}
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			render :json=> { :success => true, :message => messages }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find user' }, :status=>422
		end
	end
	def update
		begin
		@message = Message.find(params['id'])
		@user = User.find(params['user'])
		if @message.recipient_id == @user.id
			@message.is_new =  false
			@message.save!
			@user.reload
			messages = {:received =>@user.received_messages.order(created_at: :desc).limit(30),
						:sent =>@user.sent_messages.order(created_at: :desc).limit(30)}
		else
			raise 'could not find message or user'
		end
		rescue Exception => e
			error = 'could not find message'
		end
		if error.nil?
			render :json=> { :success => true, :message => messages }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find user' }, :status=>422
		end
	end
	# def destroy
	# 	begin
	# 	if @message = Message.find(params['id']) then
	# 		@user = User.find(params['user'])
	# 		#test token here!!!!!
	# 		if @user.id ==
			
	# 	else
	# 		raise 'could not find message'
	# 	end
	# 	rescue Exception => e
	# 		error = 'could not find message'
	# 	end
	# 	if error.nil?
	# 		render :json=> { :success => true, :message => messages }, :status=>201
	# 	else
	# 		render :json=> { :success => false, :error => 'could not find user' }, :status=>422
	# 	end
	# end



	def message_params
		params.require(:message).permit(:subject,:body,:is_new,:sender_id,:recipient_id)
	end
end    
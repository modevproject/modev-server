class Api::V1::ConnectionsController < Api::V1::BaseApiController
	before_action :is_token_valid?


	def show
		begin
			@user = User.find(params['id'])
		rescue Exception => e
			error = 'could not find user'
		end
		if error.nil?
			render :json=> { :success => true, :connections => @user.friends.as_json }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find connections' }, :status=>422
		end
	end
	def create
		begin
			@user = User.find(params['user'])
			@friend = User.find(params['friend'])
			if !(@user.friends.include? @friend)
				@user.friends << @friend
				@user.save
			end
		rescue Exception => e
			error = 'could not find user or connection'
		end
		if error.nil?
			render :json=> { :success => true, :connections => User.find(params['user']).friends.as_json }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find connections' }, :status=>422
		end

	end
	def destroy
		begin
			@user = User.find(params['id'])
			@friend = User.find(params['friend'])
			if @user.friends.include? @friend
				@user.friends.delete(@friend)
				@user.save
			end
		rescue Exception => e
			error = 'could not find user or connection'
		end
		if error.nil?
			render :json=> { :success => true, :connections => User.find(params['id']).friends.as_json }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find connections' }, :status=>422
		end
	end
	private
	def user_params
		params.require(:user).permit(:first_name,:last_name,:title,:company,:avatar)
	end
end

class Api::V1::CheckInsController < Api::V1::BaseApiController
	# before_action :is_token_valid?
	def create
		begin
		@user = User.find(params['user'])
		@session = EventSession.includes(:checked_in_users).find(params['session'])
		if !@session.checked_in_users.include? @user
			@session.checked_in_users << @user
			@session.save
		else
			raise 'user already checked in'
		end
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			@event_sessions = EventSession.includes(:location,:checked_in_users,:speakers).all
			@sessions = format_sessions @event_sessions
			render :json=> { :success => true, :event_session => @sessions.as_json }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find user' }, :status=>422
		end
	end
	def destroy
		begin
		@user = User.find(params['id'])
		@session = EventSession.includes(:checked_in_users).find(params['session'])
		if @session.checked_in_users.include? @user
			@session.checked_in_users.delete(@user)
			@session.save
		else
			raise 'user already checked in'
		end
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			@event_sessions = EventSession.includes(:location,:checked_in_users,:speakers).all
			@sessions = format_sessions @event_sessions
			render :json=> { :success => true, :event_session => @sessions.as_json }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find user' }, :status=>422
		end		
	end
end


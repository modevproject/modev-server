class Api::V1::SpeakersController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@speakers = Speaker.all
		render :json => @speakers.as_json
	end
	def show
		begin
			@user = Speaker.find(params['id'])
		rescue Exception => e
			error = 'could not update'
		end
		if error.nil?
			render :json=> { :success => true, :user => Speaker.find(params['id']) }, :status=>201
		else
			render :json=> { :success => false, :error => 'could not find profile' }, :status=>422
		end
	end
end

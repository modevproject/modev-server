class Api::V1::LocationsController < Api::V1::BaseApiController
	before_action :is_token_valid?

	def index
		@locations = Location.all
		render :json => @locations.as_json
	end
	def show
		begin
		@location = Location.find(params['id'])
		render :json => @location.as_json
		rescue Exception => e
		render :json=> {:success=>false, :message=>"Error Location not found"}, :status=>401
		end
	end
end

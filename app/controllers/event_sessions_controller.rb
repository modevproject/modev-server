class EventSessionsController < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@event_sessions = EventSession.all
	end
	def new
		@event_session = EventSession.new
	end
	def show
		begin
		@event_session = EventSession.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to event_sessions_path
		end
	end
	def edit
		begin
		@event_session = EventSession.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to event_sessions_path
		end
	end
	def create
		@event_session = EventSession.new(session_params)
		begin
			@event_session.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "EventSession Created!"
			redirect_to event_sessions_path
		else
			render 'new'
		end
	end
	def update
		unless @event_session = EventSession.find(params['id'])
			flash[:notice] = "EventSession Does not exist"
			redirect_to event_sessions_path
		end
		if @event_session.update_attributes session_params
			flash[:notice] = "EventSession Updated"
			redirect_to event_sessions_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_event_session_path(@event_session)
		end
	end
	def destroy
		if @event_session = EventSession.find(params['id']) then
			@event_session.destroy
			flash[:notice] = "EventSession Deleted!"
		end
		redirect_to event_sessions_path
	end
	private
	def session_params
		params.require(:event_session).permit(:name,:description,:color,:start_date,:duration,:event_id,:physical_location,:location_id,:speaker_ids => [])
	end
end

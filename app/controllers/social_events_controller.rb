class SocialEventsController  < ApplicationController
	layout "dash"
	before_action :is_user_logged_in?
	def index
		@social_events = SocialEvent.all
	end
	def new
		@social_event = SocialEvent.new
	end
	def show
		begin
		@social_event = SocialEvent.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to social_events_path
		end
	end
	def edit
		begin
		@social_event = SocialEvent.find(params['id'])
		rescue Exception => e
			flash[:alert] = e
			redirect_to social_events_path
		end
	end
	def create
		@social_event = SocialEvent.new(social_params)
		begin
			@social_event.save!
		rescue Exception => e
			flash[:error] = e
		end
		if flash[:error].nil?
			flash[:notice] = "Social Created!"
			redirect_to social_events_path
		else
			render 'new'
		end
	end
	def update
		unless @social_event = SocialEvent.find(params['id'])
			flash[:notice] = "Social Does not exist"
			redirect_to social_event_index_path
		end
		if @social_event.update_attributes social_params
			flash[:notice] = "Social Updated"
			redirect_to social_events_path
		else
			flash[:error] = "Someting Went wrong"
			redirect_to edit_social_event_path(@social_event)
		end
	end
	def destroy
		if @social_event = SocialEvent.find(params['id']) then
			@social_event.destroy
			flash[:notice] = "Social Deleted!"
		end
		redirect_to social_events_path
	end
	private
	def social_params
		params.require(:social_event).permit(:title,:body)
	end
end
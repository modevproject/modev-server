json.array!(@hello_worlds) do |hello_world|
  json.extract! hello_world, :id, :name
  json.url hello_world_url(hello_world, format: :json)
end

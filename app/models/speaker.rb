class Speaker < ActiveRecord::Base
	attr_accessor :profile_image_url
	has_and_belongs_to_many :event_sessions
	has_attached_file :profile_image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/default-profile-image.png"
	validates_attachment_content_type :profile_image, :content_type => /\Aimage\/.*\Z/
	def attributes
	  super.merge('profile_image_url' => self.profile_image_url)
	end
	after_initialize do
	  self.profile_image_url = self.profile_image.url
	end

	def name
		"#{self.first_name} #{self.last_name}"
	end
end

class TicketRegistration < ActiveRecord::Base
	def self.get_structure
		{
		:email => :email,
		:first_name => :first_name,
		:last_name => :last_name,
		:job_title => :job_title,
		:company => :company,
		:cancelled => :cancelled,
		:ticket_type => :ticket_type,
		:ticket_offer => :ticket_offer,
		:twitter_username => :twitter_username,
		:phone => :phone,
		:confirmation_number => :confirmation_number,
		:what_profession_best_describes_you => :what_profession_best_describes_you,
		:which_industry_are_you_in => :which_industry_are_you_in,
		:company_size => :company_size,
		:my_role => :my_role_for_making_purchasing_decisions_for_technology_products__services_is,
		:what_are_your_ux__design_tools_of_choice => :what_are_your_ux__design_tools_of_choice,
		:sign_up_sms_modevux_updates => :would_you_like_to_sign_up_to_receive_modevux_updates_via_sms,
		:phone_number => :phone_number,
		:food_allergies => :do_you_have_any_food_allergies_or_other_requirements_we_should_be_aware_of,
		:sponsors_follow_up => :may_our_sponsors_follow_up_with_you_after_the_conference,
		:i_heard_about_modevux_via => :i_heard_about_modevux_via,
		:referred_by => :if_someone_referred_you_to_modevux_let_us_give_them_the_credit_enter_their_name_below,
		:current_position => :my_current_position_is,
		:purchase_id => :purchase_id,
		:purchase_billing_first_name => :purchase_billing_first_name,
		:purchase_billing_last_name => :purchase_billing_last_name,
		:purchase_billing_company => :purchase_billing_company,
		:purchase_billing_email => :purchase_billing_email,
		:purchase_billing_street_address => :purchase_billing_street_address,
		:purchase_billing_postal_code => :purchase_billing_postal_code }
	end
	def self.build_me row
		ticket = TicketRegistration.new
		self.get_structure.each do |key, value|
			ticket[key] = row[value]
		end
		ticket
	end

	def self.find_me row
	   	ticket = TicketRegistration.where(:email => row[:email]).first
	end
end

class Ticket < ActiveRecord::Base
  has_attached_file :csv_file, :default_url => "/images/default-profile-image.png"
  validates_attachment_content_type :csv_file, :content_type => /.*/
end 

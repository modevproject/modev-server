class Message < ActiveRecord::Base
  belongs_to :sender, :class_name => "User"
  belongs_to :recipient, :class_name => "User"
  attr_accessor :sender_profile
  def attributes
    super.merge('sender_profile' => self.sender_profile)
  end
  after_initialize do
  	if !self.sender.nil?
    self.sender_profile = {:name => self.sender.name, :avatar => self.sender.profile_image_url}
    else
    self.sender_profile = nil	
    end
  end
end

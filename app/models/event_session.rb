class EventSession < ActiveRecord::Base
	attr_accessor :location_name
	attr_accessor :speakers_list
	attr_accessor :checked_in_users_list
	belongs_to :event
	belongs_to :location
	has_and_belongs_to_many :users
	has_and_belongs_to_many :speakers
	has_many :check_ins
	has_many :checked_in_users,:through => :check_ins,:source => "user"
	def attributes
	    super.merge('location_name' => self.location_name,'speakers_list' => self.speakers_list,'checked_in_users_list' => self.checked_in_users_list)
	end
	after_initialize do
		# if(!self.location.nil?)
	    	self.location_name = nil
	 #    end
	    self.checked_in_users_list = Array.new
	    # self.checked_in_users.each do |user|

	    #   self.checked_in_users_list << {:id => user.id, :name => user.name, :image => user.profile_image_url}
	    # end
	    self.speakers_list = Array.new
	    # self.speakers.each do |speaker|
	    #   self.speakers_list << {:id => speaker.id, :name => speaker.name, :image => speaker.profile_image_url}
	    # end
	end
end

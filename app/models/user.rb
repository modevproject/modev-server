class User < ActiveRecord::Base
  # rolify
  attr_accessor :profile_image_url
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/default-profile-image.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  has_and_belongs_to_many :sessions, :class_name => "EventSession", :foreign_key => "user_id"
  has_and_belongs_to_many :skill_sets
  has_many :connections
  has_many :received_messages, :class_name => "Message", :foreign_key => "recipient_id"
  has_many :sent_messages, :class_name => "Message", :foreign_key => "sender_id"
  has_many :friends, :through => :connections
  has_many :check_ins
  # has_many :checked_in_sessions, :through => :check_ins, :source => "event_session"
  has_one :token, autosave: true
  has_many :ratings

  def attributes
    super.merge('profile_image_url' => self.profile_image_url)
    # super.merge('sessions' => self.sessions)
  end
  after_initialize do
    self.profile_image_url = self.avatar.url

  end

  def name
    "#{self.first_name} #{self.last_name}"
  end
  def find_by_api_token token
  	 User.where(:api_token => token).first
  end
end

require 'test_helper'

class HelloWorldsControllerTest < ActionController::TestCase
  setup do
    @hello_world = hello_worlds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hello_worlds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hello_world" do
    assert_difference('HelloWorld.count') do
      post :create, hello_world: { name: @hello_world.name }
    end

    assert_redirected_to hello_world_path(assigns(:hello_world))
  end

  test "should show hello_world" do
    get :show, id: @hello_world
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hello_world
    assert_response :success
  end

  test "should update hello_world" do
    patch :update, id: @hello_world, hello_world: { name: @hello_world.name }
    assert_redirected_to hello_world_path(assigns(:hello_world))
  end

  test "should destroy hello_world" do
    assert_difference('HelloWorld.count', -1) do
      delete :destroy, id: @hello_world
    end

    assert_redirected_to hello_worlds_path
  end
end

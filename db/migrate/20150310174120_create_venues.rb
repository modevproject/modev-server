class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :title
      t.text :body

      t.timestamps null: false
    end
  end
end

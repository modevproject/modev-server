class RemoveAdressFromLocation < ActiveRecord::Migration
  def change
    remove_column :locations, :address1, :string
  end
end

class CreateCheckIns < ActiveRecord::Migration
  def change
    create_table :check_ins do |t|
      t.references :user, index: true
      t.references :event_session, index: true

      t.timestamps null: false
    end
    #add_foreign_key :check_ins, :users
    #add_foreign_key :check_ins, :event_sessions
  end
end

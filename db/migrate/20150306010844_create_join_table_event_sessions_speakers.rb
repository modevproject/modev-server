class CreateJoinTableEventSessionsSpeakers < ActiveRecord::Migration
  def change
    create_join_table :event_sessions, :speakers do |t|
      # t.index [:event_session_id, :speaker_id]
      # t.index [:speaker_id, :event_session_id]
    end
  end
end

class AddDescriptionToEventSession < ActiveRecord::Migration
  def change
    add_column :event_sessions, :description, :string
  end
end

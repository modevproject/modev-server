class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :subject
      t.string :message
      t.references :sender, index: true
      t.references :recipient, index: true
      t.boolean :is_new

      t.timestamps null: false
    end
    # add_foreign_key :messages, :senders
    # add_foreign_key :messages, :recipients
  end
end

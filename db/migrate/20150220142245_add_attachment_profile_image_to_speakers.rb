class AddAttachmentProfileImageToSpeakers < ActiveRecord::Migration
  def self.up
    change_table :speakers do |t|
      t.attachment :profile_image
    end
  end

  def self.down
    remove_attachment :speakers, :profile_image
  end
end

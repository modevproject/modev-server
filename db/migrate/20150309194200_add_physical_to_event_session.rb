class AddPhysicalToEventSession < ActiveRecord::Migration
  def change
    add_column :event_sessions, :physical_location, :string
  end
end

class CreateTicketRegistrations < ActiveRecord::Migration
  def change
    create_table :ticket_registrations do |t|
      t.string :cancelled
      t.string :ticket_type
      t.string :ticket_offer
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :company
      t.string :job_title
      t.string :twitter_username
      t.string :phone
      t.string :confirmation_number
      t.string :what_profession_best_describes_you
      t.string :which_industry_are_you_in
      t.string :company_size
      t.string :my_role
      t.string :what_are_your_ux__design_tools_of_choice
      t.string :sign_up_sms_modevux_updates
      t.string :phone_number
      t.string :food_allergies
      t.string :sponsors_follow_up
      t.string :i_heard_about_modevux_via
      t.string :referred_by
      t.string :current_position
      t.string :purchase_id
      t.string :purchase_billing_first_name
      t.string :purchase_billing_last_name
      t.string :purchase_billing_company
      t.string :purchase_billing_email
      t.string :purchase_billing_street_address
      t.string :purchase_billing_postal_code

      t.timestamps null: false
    end
  end
end

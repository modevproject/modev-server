class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :title
      t.text :body

      t.timestamps null: false
    end
  end
end

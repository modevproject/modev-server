class AddTitleToUser < ActiveRecord::Migration
  def change
    add_column :users, :title, :string
    add_column :users, :company, :string
  end
end
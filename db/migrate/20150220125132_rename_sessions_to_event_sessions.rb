class RenameSessionsToEventSessions < ActiveRecord::Migration
   def change
    rename_table :sessions, :event_sessions
  end 
end

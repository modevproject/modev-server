class AddColorToEventSession < ActiveRecord::Migration
  def change
    add_column :event_sessions, :color, :string
  end
end

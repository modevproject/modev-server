class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :score
      t.references :user, index: true
      t.references :event_session, index: true

      t.timestamps null: false
    end
    # add_foreign_key :ratings, :users
    # add_foreign_key :ratings, :event_sessions
  end
end

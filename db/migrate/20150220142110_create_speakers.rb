class CreateSpeakers < ActiveRecord::Migration
  def change
    create_table :speakers do |t|
      t.string :first_name
      t.string :last_name
      t.string :company
      t.string :title

      t.timestamps null: false
    end
  end
end

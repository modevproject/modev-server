class CreateJoinTableSkillSetsUsers < ActiveRecord::Migration
  def change
    create_join_table :skill_sets, :users do |t|
      # t.index [:event_id, :location_id]
      # t.index [:location_id, :event_id]
    end
  end
end

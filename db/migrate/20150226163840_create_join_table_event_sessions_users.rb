class CreateJoinTableEventSessionsUsers < ActiveRecord::Migration
  def change
    create_join_table :event_sessions, :users do |t|
      # t.index [:event_id, :location_id]
      # t.index [:location_id, :event_id]
    end
  end
end

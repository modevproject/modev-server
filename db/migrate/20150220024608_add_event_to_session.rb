class AddEventToSession < ActiveRecord::Migration
  def change
    add_reference :sessions, :event, index: true
    add_foreign_key :sessions, :events
  end
end

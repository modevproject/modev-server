class AddIsActiveToCsvFile < ActiveRecord::Migration
  def change
    add_column :csv_files, :is_active, :boolean
  end
end

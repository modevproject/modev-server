class AddLocationToSession < ActiveRecord::Migration
  def change
    add_reference :sessions, :location, index: true
    add_foreign_key :sessions, :locations
  end
end

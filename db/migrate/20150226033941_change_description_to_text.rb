class ChangeDescriptionToText < ActiveRecord::Migration
  def up
  change_column :event_sessions, :description,  :text
  end

  def down
  change_column :event_sessions, :description,  :string
  end
end

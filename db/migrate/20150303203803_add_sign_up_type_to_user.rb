class AddSignUpTypeToUser < ActiveRecord::Migration
  def change
    add_column :users, :sign_up_type, :string
  end
end

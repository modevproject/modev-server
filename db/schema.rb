# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150311191307) do

  create_table "check_ins", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.integer  "event_session_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "check_ins", ["event_session_id"], name: "index_check_ins_on_event_session_id", using: :btree
  add_index "check_ins", ["user_id"], name: "index_check_ins_on_user_id", using: :btree

  create_table "connections", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "friend_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "csv_files", force: :cascade do |t|
    t.string   "note",              limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.boolean  "is_active",         limit: 1
  end

  create_table "event_sessions", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.datetime "start_date"
    t.time     "duration"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "event_id",          limit: 4
    t.integer  "location_id",       limit: 4
    t.text     "description",       limit: 65535
    t.string   "color",             limit: 255
    t.string   "physical_location", limit: 255
  end

  add_index "event_sessions", ["event_id"], name: "index_event_sessions_on_event_id", using: :btree
  add_index "event_sessions", ["location_id"], name: "index_event_sessions_on_location_id", using: :btree

  create_table "event_sessions_speakers", id: false, force: :cascade do |t|
    t.integer "event_session_id", limit: 4, null: false
    t.integer "speaker_id",       limit: 4, null: false
  end

  create_table "event_sessions_users", id: false, force: :cascade do |t|
    t.integer "event_session_id", limit: 4, null: false
    t.integer "user_id",          limit: 4, null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "address1",   limit: 255
    t.string   "address2",   limit: 255
    t.string   "zip",        limit: 255
    t.string   "state",      limit: 255
    t.string   "country",    limit: 255
  end

  create_table "events_locations", id: false, force: :cascade do |t|
    t.integer "event_id",    limit: 4, null: false
    t.integer "location_id", limit: 4, null: false
  end

  create_table "hello_worlds", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "address2",   limit: 255
    t.string   "state",      limit: 255
    t.string   "zip",        limit: 255
    t.string   "country",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string   "subject",      limit: 255
    t.integer  "sender_id",    limit: 4
    t.integer  "recipient_id", limit: 4
    t.boolean  "is_new",       limit: 1
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "body",         limit: 65535
  end

  add_index "messages", ["recipient_id"], name: "index_messages_on_recipient_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "ratings", force: :cascade do |t|
    t.integer  "score",            limit: 4
    t.integer  "user_id",          limit: 4
    t.integer  "event_session_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "ratings", ["event_session_id"], name: "index_ratings_on_event_session_id", using: :btree
  add_index "ratings", ["user_id"], name: "index_ratings_on_user_id", using: :btree

  create_table "skill_sets", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "skill_sets_users", id: false, force: :cascade do |t|
    t.integer "skill_set_id", limit: 4, null: false
    t.integer "user_id",      limit: 4, null: false
  end

  create_table "social_events", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "body",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "speakers", force: :cascade do |t|
    t.string   "first_name",                 limit: 255
    t.string   "last_name",                  limit: 255
    t.string   "company",                    limit: 255
    t.string   "title",                      limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "profile_image_file_name",    limit: 255
    t.string   "profile_image_content_type", limit: 255
    t.integer  "profile_image_file_size",    limit: 4
    t.datetime "profile_image_updated_at"
    t.string   "twitter",                    limit: 255
    t.text     "description",                limit: 65535
    t.integer  "user_id",                    limit: 4
  end

  create_table "sponsors", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "body",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "ticket_registrations", force: :cascade do |t|
    t.string   "cancelled",                                limit: 255
    t.string   "ticket_type",                              limit: 255
    t.string   "ticket_offer",                             limit: 255
    t.string   "first_name",                               limit: 255
    t.string   "last_name",                                limit: 255
    t.string   "email",                                    limit: 255
    t.string   "company",                                  limit: 255
    t.string   "job_title",                                limit: 255
    t.string   "twitter_username",                         limit: 255
    t.string   "phone",                                    limit: 255
    t.string   "confirmation_number",                      limit: 255
    t.string   "what_profession_best_describes_you",       limit: 255
    t.string   "which_industry_are_you_in",                limit: 255
    t.string   "company_size",                             limit: 255
    t.string   "my_role",                                  limit: 255
    t.string   "what_are_your_ux__design_tools_of_choice", limit: 255
    t.string   "sign_up_sms_modevux_updates",              limit: 255
    t.string   "phone_number",                             limit: 255
    t.string   "food_allergies",                           limit: 255
    t.string   "sponsors_follow_up",                       limit: 255
    t.string   "i_heard_about_modevux_via",                limit: 255
    t.string   "referred_by",                              limit: 255
    t.string   "current_position",                         limit: 255
    t.string   "purchase_id",                              limit: 255
    t.string   "purchase_billing_first_name",              limit: 255
    t.string   "purchase_billing_last_name",               limit: 255
    t.string   "purchase_billing_company",                 limit: 255
    t.string   "purchase_billing_email",                   limit: 255
    t.string   "purchase_billing_street_address",          limit: 255
    t.string   "purchase_billing_postal_code",             limit: 255
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string   "email",                 limit: 255
    t.string   "first_name",            limit: 255
    t.string   "last_name",             limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "csv_file_file_name",    limit: 255
    t.string   "csv_file_content_type", limit: 255
    t.integer  "csv_file_file_size",    limit: 4
    t.datetime "csv_file_updated_at"
  end

  create_table "tokens", force: :cascade do |t|
    t.string   "state",      limit: 255
    t.string   "token",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id",    limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "api_token",              limit: 255
    t.string   "title",                  limit: 255
    t.string   "company",                limit: 255
    t.string   "skills",                 limit: 255
    t.string   "description",            limit: 255
    t.string   "sign_up_type",           limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "venues", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "body",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_foreign_key "event_sessions", "events"
  add_foreign_key "event_sessions", "locations"
end
